#!/bin/sh
### BEGIN INIT INFO
# Provides:          ziproxy
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Init script for ziproxy
# Description:       This is the init script for ziproxy.
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/ziproxy
NAME=ziproxy
DESC=ziproxy

test -x $DAEMON || exit 0

. /lib/lsb/init-functions

PIDFILE=/var/run/$NAME.pid
DODTIME=1                   # Time to wait for the server to die, in seconds
                            # If this value is set too low you might not
                            # let some servers to die gracefully and
                            # 'restart' will not work

if [ ! -d /var/run ]; then
	mkdir -p /var/run
fi

# Include ziproxy defaults if available
if [ -f /etc/default/ziproxy ] ; then
	. /etc/default/ziproxy
fi
DAEMON_OPTS="$DAEMON_OPTS -d -p $PIDFILE"

case "$1" in
    start)
        log_daemon_msg "Starting $DESC" "$NAME"
        start_daemon -p $PIDFILE $DAEMON $DAEMON_OPTS
        log_end_msg $?
	;;
    stop)
        log_daemon_msg "Stopping $DESC" "$NAME"
        killproc -p $PIDFILE $DAEMON
        RETVAL=$?
        [ $RETVAL -eq 0 ] && [ -e "$PIDFILE" ] && rm -f $PIDFILE
        log_end_msg $RETVAL
	;;
    restart|reload|force-reload)
        log_daemon_msg "Restarting $DESC" "$NAME" 
        $0 stop
	[ -n "$DODTIME" ] && sleep $DODTIME
        $0 start
	;;
    status)
        status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
	;;
    *)
	N=/etc/init.d/$NAME
	log_action_msg "Usage: $N {start|stop|restart|reload|force-reload|status}"
	exit 1
	;;
esac

exit 0
